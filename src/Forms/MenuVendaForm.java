/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Forms;

import Banco.ClienteBD;
import Banco.ProdutoBD;
import Banco.VendaBD;

import Modelo.Tabela;
import Modelo.Venda;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;

/**
 *
 * @author Lucas
 */
public class MenuVendaForm extends javax.swing.JFrame {

    private int idFuncionario;
    
    public MenuVendaForm(int idFuncionario) throws SQLException, ClassNotFoundException {
        initComponents();
        this.idFuncionario = idFuncionario;
        preencherTabelaClientes();
        preencherTabelaProdutos();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        rdCPF = new javax.swing.JRadioButton();
        rdNome = new javax.swing.JRadioButton();
        txtBusca = new javax.swing.JTextField();
        txtBuscar = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        txtCliente = new javax.swing.JTextField();
        txtProduto = new javax.swing.JTextField();
        btnVender = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tableClientes = new javax.swing.JTable();
        jScrollPane4 = new javax.swing.JScrollPane();
        tableProdutos = new javax.swing.JTable();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("Efetuar Venda");

        jLabel2.setText("Buscar Clientes:");

        rdCPF.setText("CPF/CNPJ");

        rdNome.setText("Nome");

        txtBuscar.setText("Buscar");

        jLabel3.setText("Produtos:");

        txtCliente.setEditable(false);

        txtProduto.setEditable(false);

        btnVender.setText("Finalizar Venda");
        btnVender.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVenderActionPerformed(evt);
            }
        });

        jLabel4.setText("Cliente:");

        jLabel5.setText("Produto:");

        jLabel7.setText("Clientes:");

        tableClientes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane3.setViewportView(tableClientes);

        tableProdutos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane4.setViewportView(tableProdutos);

        jMenu1.setText("Sobre");
        jMenuBar1.add(jMenu1);

        jMenu2.setText("Sair");
        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel3)
                        .addGap(208, 208, 208))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel1)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel2)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(rdCPF)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(rdNome)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtBusca)
                                .addGap(18, 18, 18)
                                .addComponent(txtBuscar))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel4)
                                    .addComponent(txtCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel5)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(txtProduto, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnVender, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 269, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 236, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap(22, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtBusca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtBuscar))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel2)
                        .addComponent(rdCPF)
                        .addComponent(rdNome)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 114, Short.MAX_VALUE)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnVender)
                    .addComponent(txtProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(19, 19, 19))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnVenderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVenderActionPerformed
        VendaBD venda = new VendaBD();
        
        try {
            venda.inserirVenda(new Venda(Integer.parseInt(tableClientes.getValueAt(tableClientes.getSelectedRow(), 0).toString()), idFuncionario, Integer.parseInt(tableProdutos.getValueAt(tableProdutos.getSelectedRow(), 0).toString())));
            JOptionPane.showMessageDialog(null, "Venda efetuada com sucesso!");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(CadastroProduto.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(CadastroProduto.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnVenderActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnVender;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JRadioButton rdCPF;
    private javax.swing.JRadioButton rdNome;
    private javax.swing.JTable tableClientes;
    private javax.swing.JTable tableProdutos;
    private javax.swing.JTextField txtBusca;
    private javax.swing.JButton txtBuscar;
    private javax.swing.JTextField txtCliente;
    private javax.swing.JTextField txtProduto;
    // End of variables declaration//GEN-END:variables

    public void preencherTabelaClientes() throws SQLException, ClassNotFoundException {
        
        tableClientes.addMouseListener(new MouseAdapter() {
        @Override
        public void mouseClicked(MouseEvent arg0) {
            txtCliente.setText(tableClientes.getValueAt(tableClientes.getSelectedRow(), 0).toString());
	}});
        
        ClienteBD cliente = new ClienteBD();

        String[] colunas = new String[]{"Código", "Nome", "CPF/CNPJ", "Matrícula"};

        Tabela tab = new Tabela(cliente.listarCliente(), colunas);

        tableClientes.setModel(tab);
        tableProdutos.getColumnModel().getColumn(0).setPreferredWidth(20);
        tableProdutos.getColumnModel().getColumn(0).setResizable(false);
        tableProdutos.getColumnModel().getColumn(1).setPreferredWidth(40);
        tableProdutos.getColumnModel().getColumn(1).setResizable(false);
        tableProdutos.getColumnModel().getColumn(2).setPreferredWidth(70);
        tableProdutos.getColumnModel().getColumn(2).setResizable(false);
        tableProdutos.getColumnModel().getColumn(3).setPreferredWidth(70);
        tableProdutos.getColumnModel().getColumn(3).setResizable(false);

        tableClientes.getTableHeader().setReorderingAllowed(false);
        tableClientes.setAutoResizeMode(tableClientes.AUTO_RESIZE_OFF);
        tableClientes.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }

    public void preencherTabelaProdutos() throws SQLException, ClassNotFoundException {
        
        tableProdutos.addMouseListener(new MouseAdapter() {
        @Override
        public void mouseClicked(MouseEvent arg0) {
            txtProduto.setText(tableProdutos.getValueAt(tableProdutos.getSelectedRow(), 0).toString());
	}});
        
        ProdutoBD produto = new ProdutoBD();

        String[] colunas = new String[]{"Código", "Nome", "Preço", "Categoria"};

        Tabela tab = new Tabela(produto.listarProduto(), colunas);

        tableProdutos.setModel(tab);
        tableProdutos.getColumnModel().getColumn(0).setPreferredWidth(45);
        tableProdutos.getColumnModel().getColumn(0).setResizable(false);
        tableProdutos.getColumnModel().getColumn(1).setPreferredWidth(50);
        tableProdutos.getColumnModel().getColumn(1).setResizable(false);
        tableProdutos.getColumnModel().getColumn(2).setPreferredWidth(70);
        tableProdutos.getColumnModel().getColumn(2).setResizable(false);
        tableProdutos.getColumnModel().getColumn(3).setPreferredWidth(70);
        tableProdutos.getColumnModel().getColumn(3).setResizable(false);

        tableProdutos.getTableHeader().setReorderingAllowed(false);
        tableProdutos.setAutoResizeMode(tableClientes.AUTO_RESIZE_OFF);
        tableProdutos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }
}
