package Banco;

import Modelo.Venda;
import java.sql.Date;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class VendaBD {

    public void inserirVenda(Venda objVenda) throws ClassNotFoundException, SQLException {
        String sql = "INSERT INTO venda (\"idFuncionario\", \"idCliente\", \"idProduto\", \"data\") VALUES (?, ?, ?, ?);";

        Calendar currenttime = Calendar.getInstance();

        PreparedStatement ps = ConexaoBD.connection().prepareStatement(sql);
        ps.setInt(1, objVenda.getIdFuncionario());
        ps.setInt(2, objVenda.getIdCliente());
        ps.setInt(3, objVenda.getIdProduto());
        ps.setDate(4, new Date((currenttime.getTime()).getTime()));
        ps.execute();
        ps.close();

        ConexaoBD.connection().close();
    }

    public ArrayList listarVenda() throws ClassNotFoundException, SQLException {
        ArrayList lstVenda = new ArrayList();

        String sql = "SELECT \"idVenda\", p.\"nome\", p.\"preco\", c.\"nome\", data FROM venda as v, funcionario as f, cliente as c, produto as p\n"
                + "WHERE v.\"idCliente\" = c.\"idCliente\" AND\n"
                + "v.\"idProduto\" = p.\"idProduto\""
                + "v.\"idFuncionario\" = v.\"idFuncionario\"";

        PreparedStatement ps = ConexaoBD.connection().prepareStatement(sql);
        ResultSet rs = ps.executeQuery();

        while (rs.next()) {
            lstVenda.add(new Object[]{rs.getInt(1), rs.getString(2), rs.getBigDecimal(3), rs.getString(4), rs.getDate(5)});
        }

        ps.close();
        ConexaoBD.connection().close();

        return lstVenda;
    }
    
    public ArrayList listarVenda(int idCliente) throws ClassNotFoundException, SQLException {
        ArrayList lstVenda = new ArrayList();

        String sql = "SELECT \"idVenda\", p.\"nome\", p.\"preco\", c.\"nome\", data FROM venda as v, funcionario as f, cliente as c, produto as p\n"
                + "WHERE v.\"idCliente\" = c.\"idCliente\" AND\n"
                + "v.\"idFuncionario\" = v.\"idFuncionario\" AND\n"
                + "v.\"idProduto\" = p.\"idProduto\" AND\n"
                + " v.\"idCliente\" = ?";

        PreparedStatement ps = ConexaoBD.connection().prepareStatement(sql);
        
        ps.setInt(1, idCliente);
         
        ResultSet rs = ps.executeQuery();

        while (rs.next()) {
            lstVenda.add(new Object[]{rs.getInt(1), rs.getString(2), rs.getBigDecimal(3), rs.getString(4), rs.getDate(5)});
        }

        ps.close();
        ConexaoBD.connection().close();

        return lstVenda;
    }
}
