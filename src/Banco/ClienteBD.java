package Banco;

import Modelo.Cliente;
import java.math.BigDecimal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ClienteBD {

    public void inserirCliente(Cliente objCliente) throws ClassNotFoundException, SQLException {
        String sql = "INSERT INTO cliente (nome, cpf, email, telefone, matricula, senha, saldo) VALUES (?, ?, ?, ?, ?, ?, ?);";

        PreparedStatement ps = ConexaoBD.connection().prepareStatement(sql);
        ps.setString(1, objCliente.getNome());
        ps.setString(2, objCliente.getCpf());
        ps.setString(3, objCliente.getEmail());
        ps.setString(4, objCliente.getTelefone());
        ps.setString(5, objCliente.getMatricula());
        ps.setString(6, objCliente.getSenha());
        ps.setBigDecimal(7, objCliente.getSaldo());
        ps.execute();
        ps.close();

        ConexaoBD.connection().close();
    }

    public void removerCliente(Cliente objCliente) throws ClassNotFoundException, SQLException {
        String sql = "DELETE FROM cliente WHERE idCliente = ?;";

        PreparedStatement ps = ConexaoBD.connection().prepareStatement(sql);
        ps.setInt(1, objCliente.getId());
        ps.execute();
        ps.close();

        ConexaoBD.connection().close();
    }

    public BigDecimal retornaNovoSaldo(int idCliente) throws ClassNotFoundException, SQLException {
        String sql = "SELECT saldo FROM cliente WHERE \"idCliente\" = ?";

        PreparedStatement ps = ConexaoBD.connection().prepareStatement(sql);
        ps.setInt(1, idCliente);
        ResultSet rs = ps.executeQuery();

        if (rs.next()) {
            return rs.getBigDecimal(1);
        }

        ps.close();
        ConexaoBD.connection().close();

        return null;
    }

    public void alterarCliente(Cliente objCliente) throws ClassNotFoundException, SQLException {
        String sql = "UPDATE cliente SET nome = ?, cpf = ?, email = ?, telefone = ?, matricula = ?, senha = ?, saldo = ? WHERE idCliente = ?;";

        PreparedStatement ps = ConexaoBD.connection().prepareStatement(sql);
        ps.setInt(1, objCliente.getId());
        ps.setString(2, objCliente.getNome());
        ps.setString(3, objCliente.getCpf());
        ps.setString(4, objCliente.getEmail());
        ps.setString(5, objCliente.getTelefone());
        ps.setString(6, objCliente.getMatricula());
        ps.setString(7, objCliente.getSenha());
        ps.setBigDecimal(8, objCliente.getSaldo());
        ps.execute();
        ps.close();

        ConexaoBD.connection().close();
    }

    public void alterarSaldoCliente(int idCliente, BigDecimal saldo) throws ClassNotFoundException, SQLException {
        String sql = "UPDATE cliente SET saldo = ? WHERE \"idCliente\" = ?;";

        PreparedStatement ps = ConexaoBD.connection().prepareStatement(sql);
        ps.setBigDecimal(1, saldo);
        ps.setInt(2, idCliente);
        ps.execute();
        ps.close();

        ConexaoBD.connection().close();
    }

    public ArrayList listarCliente() throws ClassNotFoundException, SQLException {
        ArrayList lstCliente = new ArrayList();

        String sql = "SELECT * FROM cliente";

        PreparedStatement ps = ConexaoBD.connection().prepareStatement(sql);
        ResultSet rs = ps.executeQuery();

        while (rs.next()) {
            lstCliente.add(new Object[]{rs.getInt("idCliente"), rs.getString("nome"), rs.getString("cpf"), rs.getString("matricula")});
        }

        ps.close();
        ConexaoBD.connection().close();

        return lstCliente;
    }

    public ArrayList listarClienteTransferencia(int idCliente) throws ClassNotFoundException, SQLException {
        ArrayList lstCliente = new ArrayList();

        String sql = "SELECT * FROM cliente WHERE \"idCliente\" != ?;";

        PreparedStatement ps = ConexaoBD.connection().prepareStatement(sql);
        ps.setInt(1, idCliente);
        ResultSet rs = ps.executeQuery();

        while (rs.next()) {
            lstCliente.add(new Object[]{rs.getInt("idCliente"), rs.getString("nome"), rs.getString("cpf"), rs.getBigDecimal("saldo")});
        }

        ps.close();
        ConexaoBD.connection().close();

        return lstCliente;
    }
}
