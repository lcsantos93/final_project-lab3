package Banco;

import Modelo.Produto;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProdutoBD {

    public void inserirProduto(Produto objProduto) throws ClassNotFoundException, SQLException {
        String sql = "INSERT INTO produto (nome, preco, categoria) VALUES (?, ?, ?);";

        PreparedStatement ps = ConexaoBD.connection().prepareStatement(sql);
        ps.setString(1, objProduto.getNome());
        ps.setBigDecimal(2, objProduto.getPreco());
        ps.setString(3, objProduto.getCategoria());
        ps.execute();
        ps.close();

        ConexaoBD.connection().close();
    }

    public void removerProduto(Produto objProduto) throws ClassNotFoundException, SQLException {
        String sql = "DELETE FROM produto WHERE idProduto = ?;";

        PreparedStatement ps = ConexaoBD.connection().prepareStatement(sql);
        ps.setInt(1, objProduto.getId());
        ps.execute();
        ps.close();

        ConexaoBD.connection().close();
    }

    public void alterarProduto(Produto objProduto) throws ClassNotFoundException, SQLException {
        String sql = "UPDATE produto SET nome = ?, preco = ?, categoria = ? WHERE idProduto = ?;";

        PreparedStatement ps = ConexaoBD.connection().prepareStatement(sql);
        ps.setInt(1, objProduto.getId());
        ps.setString(2, objProduto.getNome());
        ps.setBigDecimal(3, objProduto.getPreco());
        ps.setString(4, objProduto.getCategoria());
        ps.execute();
        ps.close();

        ConexaoBD.connection().close();
    }

    public ArrayList listarProduto() throws ClassNotFoundException, SQLException {
        ArrayList lstProduto = new ArrayList();

        String sql = "SELECT * FROM produto";

        PreparedStatement ps = ConexaoBD.connection().prepareStatement(sql);
        ResultSet rs = ps.executeQuery();

        while (rs.next()) {
            lstProduto.add(new Object[]{rs.getInt("idProduto"), rs.getString("nome"), rs.getString("preco"), rs.getString("categoria")});
        }

        ps.close();
        ConexaoBD.connection().close();

        return lstProduto;
    }
}
