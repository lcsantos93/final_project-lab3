package Banco;

import Modelo.Funcionario;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class FuncionarioBD {

    public void inserirFuncionario(Funcionario objFuncionario) throws ClassNotFoundException, SQLException {
        String sql = "INSERT INTO funcionario (nome, cpf, email, telefone, senha) VALUES (?, ?, ?, ?, ?);";

        PreparedStatement ps = ConexaoBD.connection().prepareStatement(sql);
        ps.setString(1, objFuncionario.getNome());
        ps.setString(2, objFuncionario.getCpf());
        ps.setString(3, objFuncionario.getEmail());
        ps.setString(4, objFuncionario.getTelefone());
        ps.setString(5, objFuncionario.getSenha());
        ps.execute();
        ps.close();

        ConexaoBD.connection().close();
    }

    public void removerFuncionario(Funcionario objFuncionario) throws ClassNotFoundException, SQLException {
        String sql = "DELETE FROM funcionario WHERE idFuncionario = ?;";

        PreparedStatement ps = ConexaoBD.connection().prepareStatement(sql);
        ps.setInt(1, objFuncionario.getId());
        ps.execute();
        ps.close();

        ConexaoBD.connection().close();
    }

    public void alterarFuncionario(Funcionario objFuncionario) throws ClassNotFoundException, SQLException {
        String sql = "UPDATE funcionario SET nome = ?, cpf = ?, email = ?, telefone = ?, senha = ? WHERE idFuncionario = ?;";

        PreparedStatement ps = ConexaoBD.connection().prepareStatement(sql);
        ps.setInt(1, objFuncionario.getId());
        ps.setString(2, objFuncionario.getNome());
        ps.setString(3, objFuncionario.getCpf());
        ps.setString(4, objFuncionario.getEmail());
        ps.setString(5, objFuncionario.getTelefone());
        ps.setString(6, objFuncionario.getSenha());
        ps.execute();
        ps.close();

        ConexaoBD.connection().close();
    }

    public ArrayList listarFuncionario() throws ClassNotFoundException, SQLException {
        ArrayList lstFuncionario = new ArrayList();

        String sql = "SELECT * FROM funcionario";

        PreparedStatement ps = ConexaoBD.connection().prepareStatement(sql);
        ResultSet rs = ps.executeQuery();

        while (rs.next()) {
            lstFuncionario.add(new Object[]{rs.getInt("idFuncionario"), rs.getString("nome"), rs.getString("cpf"), rs.getString("telefone")});
        }

        ps.close();
        ConexaoBD.connection().close();

        return lstFuncionario;
    }
}
