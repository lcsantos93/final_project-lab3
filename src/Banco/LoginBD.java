package Banco;

import Modelo.Cliente;
import Modelo.Funcionario;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class LoginBD {

    public Cliente verificarUsuarioCliente(String email, String senha) throws ClassNotFoundException, SQLException {
        String sql = "SELECT * FROM cliente WHERE email = ? and senha = ?";

        PreparedStatement ps = ConexaoBD.connection().prepareStatement(sql);
        
        ps.setString(1, email);
	ps.setString(2, senha);
        
        ResultSet rs = ps.executeQuery();

        Cliente objCliente = null;

        while (rs.next()) {
            objCliente = new Cliente();

            objCliente.setId(rs.getInt("idCliente"));
            objCliente.setNome(rs.getString("nome"));
            objCliente.setCpf(rs.getString("cpf"));
            objCliente.setEmail(rs.getString("email"));
            objCliente.setTelefone(rs.getString("telefone"));
            objCliente.setMatricula(rs.getString("matricula"));
            objCliente.setSaldo(rs.getBigDecimal("saldo"));
        }

        ps.close();
        ConexaoBD.connection().close();

        return objCliente;
    }

    public Funcionario verificarUsuarioFuncionario(String email, String senha) throws ClassNotFoundException, SQLException {
        String sql = "SELECT * FROM funcionario WHERE email = ? and senha = ?";

        PreparedStatement ps = ConexaoBD.connection().prepareStatement(sql);
        
        ps.setString(1, email);
	ps.setString(2, senha);
        
        ResultSet rs = ps.executeQuery();

        Funcionario objFuncionario= null;

        while (rs.next()) {
            objFuncionario = new Funcionario();

            objFuncionario.setId(rs.getInt("idFuncionario"));
            objFuncionario.setNome(rs.getString("nome"));
            objFuncionario.setCpf(rs.getString("cpf"));
            objFuncionario.setEmail(rs.getString("email"));
            objFuncionario.setTelefone(rs.getString("telefone"));
        }

        ps.close();
        ConexaoBD.connection().close();

        return objFuncionario;
    }
}
