package Banco;

import java.sql.DriverManager;
import java.sql.SQLException;

import java.sql.Connection;

public class ConexaoBD {  
	public static Connection connection() throws SQLException, ClassNotFoundException{
		Class.forName("org.postgresql.Driver");
		
		String url = "jdbc:postgresql://localhost:5432/TrabalhoFinal", user = "postgres", password = "lcs1611";
		
		Connection connection = DriverManager.getConnection(url, user, password);
		
		return connection;
	}
	
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		connection();
	}
}    