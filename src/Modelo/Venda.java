package Modelo;

import java.io.Serializable;
import java.sql.Date;

public class Venda  implements Serializable{
    
    private static final long serialVersionUID = -706110636496987562L;
    private int id;
    private int idFuncionario;
    private int idCliente;
    private int idProduto;
    private Date data;
    
    public Venda() {
        this.id = 0;
        this.idFuncionario = 0;
        this.idCliente = 0;
        this.idProduto = 0;
        this.data = null;
    }
    
    public Venda(int idCliente, int idFuncionario, int idProduto) {
        this.idFuncionario = idFuncionario;
        this.idCliente = idCliente;
        this.idProduto = idProduto;
    }
    
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public int getIdFuncionario() {
        return idFuncionario;
    }
    
    public void setIdFuncionario(int idFuncionario) {
        this.idFuncionario = idFuncionario;
    }
    
    public int getIdCliente() {
        return idCliente;
    }
    
    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }
    
    public int getIdProduto() {
        return idProduto;
    }
    
    public void setIdProduto(int idProduto) {
        this.idProduto = idProduto;
    }
    
    public Date getData() {
        return data;
    }
    public void setData(Date data) {
        this.data = data;
    }
}