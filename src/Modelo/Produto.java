
package Modelo;

import java.io.Serializable;
import java.math.BigDecimal;

public class Produto  implements Serializable{
    
    private static final long serialVersionUID = -706110636496987562L;
    private int id;
    private String nome;
    private BigDecimal preco;
    private String categoria;
    
    public Produto() {
        this.id = 0;
        this.nome = "";
        this.preco = new BigDecimal(0);
        this.categoria = "";
    }

    public Produto(String nome, BigDecimal preco, String categoria) {
        this.nome = nome;
        this.preco = preco;
        this.categoria = categoria;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public BigDecimal getPreco() {
        return preco;
    }

    public void setPreco(BigDecimal preco) {
        this.preco = preco;
    }
    
    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }
}
